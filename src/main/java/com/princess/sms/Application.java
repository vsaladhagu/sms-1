package com.princess.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * 
 * @version $Revision: 1.0 $
 */
@SpringBootApplication
@EnableAutoConfiguration
public class Application {

  /**
   * Method main.
   * 
   * @param args String[]
   */
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  /**
   * Method restTemplate.
   * 
   * 
   * @return RestTemplate
   */
  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

}
