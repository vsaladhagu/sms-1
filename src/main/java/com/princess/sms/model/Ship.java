package com.princess.sms.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@XmlRootElement(name = "ship")
public class Ship extends AbstractBaseObject {


  /**
   * 
   */
  private static final long serialVersionUID = 2246697217069582231L;
  private String code;
  private String name;

  /**
   * Method getCode.
   * 
   * 
   * 
   * @return String
   */
  public String getCode() {
    return code;
  }

  /**
   * Method getName.
   * 
   * 
   * 
   * @return String
   */
  public String getName() {
    return name;
  }

  /**
   * Method setCode.
   * 
   * @param code String
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * Method setName.
   * 
   * @param name String
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Method equals.
   * 
   * @param object Object
   * @return boolean
   */
  @Override
  public boolean equals(Object object) {
    if (!(object instanceof Ship)) {
      return false;
    }
    Ship rhs = (Ship) object;
    return new EqualsBuilder().append(this.code, rhs.code).isEquals();
  }

  /**
   * Method hashCode.
   * 
   * @return int
   */
  @Override
  public int hashCode() {
    return new HashCodeBuilder(1, 31).append(code).toHashCode();
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("code", this.code).toString();
  }
}
