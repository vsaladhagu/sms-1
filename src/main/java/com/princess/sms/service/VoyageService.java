package com.princess.sms.service;

import com.princess.sms.model.Voyage;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
public interface VoyageService {

  /**
   * Method findCurrentVoyage.
   * 
   * 
   * 
   * @return Voyage
   */
  public Voyage findCurrentVoyage();

}
