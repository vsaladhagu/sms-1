package com.princess.sms;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * 
 * @version $Revision: 1.0 $
 */
public enum ExceptionCode {
  CURRENT_SHIP_NOT_FOUND(
      "ship.not.found",
      HttpStatus.SERVICE_UNAVAILABLE,
      8002,
      "Ship cannot be found"),
  CURRENT_VOYAGE_NOT_FOUND(
      "voyage.not.found",
      HttpStatus.SERVICE_UNAVAILABLE,
      8002,
      "Current Voyage cannot be found"),
  SUPPORTED_LANGUAGES_NOT_FOUND(
      "supported.languages.not.found",
      HttpStatus.SERVICE_UNAVAILABLE,
      8002,
      "Supported Languages cannot be found"),;

  private int code;
  private String description = "test";
  private HttpStatus httpStatus;
  private String key;

  /**
   * 
   * @param key
   * @param description
   * @param httpStatus HttpStatus
   * @param code int
   */
  private ExceptionCode(String key, HttpStatus httpStatus, int code, String description) {
    this.key = key;
    this.description = description;
    this.code = code;
    this.httpStatus = httpStatus;
  }

  /**
   * Method getCode.
   * 
   * 
   * 
   * @return int
   */
  public int getCode() {
    return code;
  }

  /**
   * Method getDescription.
   * 
   * 
   * 
   * @return String
   */
  public String getDescription() {
    return description;
  }

  /**
   * Method getHttpStatus.
   * 
   * 
   * 
   * @return HttpStatus
   */
  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

  /**
   * Method getKey.
   * 
   * 
   * 
   * @return String
   */
  public String getKey() {
    return key;
  }

  /**
   * Method toString.
   * 
   * 
   * 
   * 
   * @return String
   */
  @Override
  public String toString() {
    return key;
  }
}
