package com.princess.sms.webapp.controller;

import java.net.URL;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.princess.sms.Application;
import com.princess.sms.model.Voyage;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest({"server.port=0"})
public class VoyageControllerTestIT {
  private static final Logger LOGGER = LoggerFactory.getLogger(VoyageControllerTestIT.class);

  @Value("${local.server.port}")
  private int port;

  private URL base;
  private RestTemplate template;

  @Before
  public void setUp() throws Exception {
    this.base = new URL("http://localhost:" + port + "/voyages");
    template = new TestRestTemplate();
  }

  @Test
  public void testFindCurrentVoyage() throws Exception {
    ResponseEntity<String> response = template.getForEntity(base.toString() + "/current", String.class);
    Voyage ob = new ObjectMapper().readValue(response.getBody(), Voyage.class);

    Assert.assertEquals("H446", ob.getVoyageNumber());
  }
}
